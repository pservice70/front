import Vue from 'vue'
//import BootstrapVue from 'bootstrap-vue'
import {Layout, Navbar, FormInput, Button, ListGroup, Card, Tabs, Table} from 'bootstrap-vue/es/components'

Vue.use(Layout);
Vue.use(Navbar);
Vue.use(FormInput);
Vue.use(Button);
Vue.use(ListGroup);
Vue.use(Card);
Vue.use(Tabs);
Vue.use(Table);


import './utils/fontAwesome'

import App from './App.vue'
import store from './store'
import router from './router/router.js'

Vue.config.productionTip = false;
//Vue.use(BootstrapVue);

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import './sass/styles.scss'

new Vue({
    render: h => h(App),
    router,
    store
}).$mount('#app');
