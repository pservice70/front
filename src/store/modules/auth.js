/* eslint-disable promise/param-names */
import {AUTH_REQUEST, AUTH_ERROR, AUTH_SUCCESS, AUTH_LOGOUT} from '../actions/auth'
import {axios} from '../../utils/api.js'
import {APP_STATE_REQUEST} from "../actions/app";

const state = {
    token: localStorage.getItem('user-token') || '',
    status: '',
    hasLoadedOnce: false
};

const getters = {
    isAuthenticated: state => !!state.token,
    authStatus: state => state.status,
};


const actions = {
    [AUTH_REQUEST]: ({commit, dispatch}, user) => {
        return new Promise((resolve, reject) => {
            commit(AUTH_REQUEST);

            axios.post('/api/login', user)
                .then(resp => {
                        const token = resp.data.success.token;
                        commit(AUTH_SUCCESS, token);

                        dispatch(APP_STATE_REQUEST);

                        resolve(resp)
                    }
                )
                .catch(err => {
                    commit(AUTH_ERROR, err);
                    localStorage.removeItem('user-token');
                    reject(err)
                });

        })
    },
    [AUTH_LOGOUT]: ({commit}) => {
        return new Promise((resolve) => {
            commit(AUTH_LOGOUT);
            axios.defaults.headers.common['Authorization'] = '';
            localStorage.removeItem('user-token');
            resolve()
        })
    }
};

const mutations = {
    [AUTH_REQUEST]: (state) => {
        state.status = 'loading'
    },
    [AUTH_SUCCESS]: (state, token) => {
        state.status = 'success';
        state.token = token;
        state.hasLoadedOnce = true;

        // Сохраняем в localStorage токен, чтобы потом не просить вход заново при обновлении приложения
        localStorage.setItem('user-token', token);
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    },
    [AUTH_ERROR]: (state) => {
        state.status = 'error';
        state.hasLoadedOnce = true
    },
    [AUTH_LOGOUT]: (state) => {
        state.token = ''
    }
};

export default {
    state,
    getters,
    actions,
    mutations,
}