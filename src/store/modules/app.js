import axios from 'axios';
import {APP_STATE_ERROR, APP_STATE_REQUEST, APP_STATE_SUCCESS} from "../actions/app";
import {USER_SUCCESS} from "../actions/user";

const state = {
    status: '',
    server: '',
};
const actions = {

    [APP_STATE_REQUEST]: ({commit}) => {
        commit(APP_STATE_REQUEST);
        axios.get('/api/state')
            .then(resp => {
                    let user = resp.data.user;

                    commit(USER_SUCCESS, user);
                    commit(APP_STATE_SUCCESS);
                }
            )
            .catch(error => {
                console.log('Error', error);
                commit(APP_STATE_ERROR);
            });
    }

};

const mutations = {
    [APP_STATE_REQUEST]: (state) => {
        state.status = 'loading'
    },
    [APP_STATE_SUCCESS]: (state) => {
        state.status = 'success';
    },
    [APP_STATE_ERROR]: (state) => {
        state.status = 'error';
    }
};

export default {
    state,
    actions,
    mutations,
}