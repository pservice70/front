import {USER_REQUEST, USER_ERROR, USER_SUCCESS} from '../actions/user'
import Vue from 'vue'
import {AUTH_LOGOUT} from '../actions/auth'

const state = {
    status: '',
    profile: {
        name: '',
        email: ''
    }
};

const getters = {
    getProfile: state => state.profile,
    isProfileLoaded: state => state.profile && !!state.profile.name,
    userName: state => state.profile  ? state.profile.name: ''
};

const actions = {};

const mutations = {
    [USER_REQUEST]: (state) => {
        state.status = 'loading'
    },
    [USER_SUCCESS]: (state, resp) => {
        state.status = 'success';
        Vue.set(state, 'profile', resp)
    },
    [USER_ERROR]: (state) => {
        state.status = 'error'
    },
    [AUTH_LOGOUT]: (state) => {
        state.profile = {name:'',email:''}
    }
};

export default {
    state,
    getters,
    actions,
    mutations,
}