import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'

import Login from '../components/login/login.vue'
import Register from '../components/login/register'
import Account from '../components/account/index.vue'
import Statistics from '../components/statistics/StatisticsPage.vue'
import StopsIndex from "../components/stops/StopsIndex";
import StopsCreate from "../components/stops/StopsCreate";
import StopsImport from "../components/stops/StopsImport";
import RoutesIndex from "../components/routes/RoutesIndex";
import RoutesCreate from "../components/routes/RoutesCreate";
import RoutesDetail from "../components/routes/RoutesDetail";
import RoutesStopsImport from "../components/routes/RoutesStopsImport";
import StopsEdit from "../components/stops/StopsEdit";
import VehicleTypesIndex from "../components/vehicle_types/VehicleTypesIndex";
import VehicleTypesCreate from "../components/vehicle_types/VehicleTypesCreate";
import VehicleTypesEdit from "../components/vehicle_types/VehicleTypesEdit";
import VehicleIndex from "../components/vehicles/VehicleIndex";
import VehicleCreate from "../components/vehicles/VehicleCreate";
import VehicleEdit from "../components/vehicles/VehicleEdit";
import TerminalsIndex from "../components/terminals/TerminalsIndex";
import TerminalsCreate from "../components/terminals/TerminalsCreate";
import TerminalsEdit from "../components/terminals/TerminalsEdit";
import CardsIndex from "../components/cards/CardsIndex";
import CardsImport from "../components/cards/CardsImport";
import TransactionsIndex from "../components/transactions/TransactionsIndex";
import TransactionsImport from "../components/transactions/TransactionsImport";
import RoutesEdit from "../components/routes/RoutesEdit";
import StatisticsByHour from "../components/statistics/StatisticsByHour.vue";
import StatisticsByTotal from "../components/statistics/StatisticsByTotal";

Vue.use(VueRouter);

const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters.isAuthenticated) {
        next();
        return
    }
    next('/')
};

const ifAuthenticated = (to, from, next) => {
    if (store.getters.isAuthenticated) {
        next();
        return
    }
    next('/login')
};

const firstRoute = (to, from, next) => {
    if (store.getters.isAuthenticated) {
        next('/statistics/total');
    } else {
        next('/login');
    }
};


const routes = [
    {path: '/', beforeEnter: firstRoute},

    {path: '/login', component: Login, beforeEnter: ifNotAuthenticated},
    {path: '/register', component: Register, beforeEnter: ifNotAuthenticated},
    {path: '/account', component: Account, beforeEnter: ifAuthenticated},
    {path: '/statistics', component: Statistics, beforeEnter: ifAuthenticated},
    {path: '/statistics/by-hour', component: StatisticsByHour, beforeEnter: ifAuthenticated},
    {path: '/statistics/total', component: StatisticsByTotal, beforeEnter: ifAuthenticated},

    {path: '/stops', component: StopsIndex, beforeEnter: ifAuthenticated},
    {path: '/stops/create', component: StopsCreate, beforeEnter: ifAuthenticated},
    {path: '/stops/edit/:id', component: StopsEdit, beforeEnter: ifAuthenticated},

    {path: '/stops/import', component: StopsImport, beforeEnter: ifAuthenticated},

    {path: '/routes', component: RoutesIndex, beforeEnter: ifAuthenticated},
    {path: '/routes/create', component: RoutesCreate, beforeEnter: ifAuthenticated},
    {path: '/routes/:id/detail', component: RoutesDetail, beforeEnter: ifAuthenticated},
    {path: '/routes/edit/:id', component: RoutesEdit, beforeEnter: ifAuthenticated},
    {path: '/routes/:id/import', component: RoutesStopsImport, beforeEnter: ifAuthenticated},


    {path: '/vehicle-types', component: VehicleTypesIndex, beforeEnter: ifAuthenticated},
    {path: '/vehicle-types/create', component: VehicleTypesCreate, beforeEnter: ifAuthenticated},
    {path: '/vehicle-types/edit/:id', component: VehicleTypesEdit, beforeEnter: ifAuthenticated},

    {path: '/vehicles', component: VehicleIndex, beforeEnter: ifAuthenticated},
    {path: '/vehicles/create', component: VehicleCreate, beforeEnter: ifAuthenticated},
    {path: '/vehicles/edit/:id', component: VehicleEdit, beforeEnter: ifAuthenticated},

    {path: '/terminals', component: TerminalsIndex, beforeEnter: ifAuthenticated},
    {path: '/terminals/create', component: TerminalsCreate, beforeEnter: ifAuthenticated},
    {path: '/terminals/edit/:id', component: TerminalsEdit, beforeEnter: ifAuthenticated},

    {path: '/cards', component: CardsIndex, beforeEnter: ifAuthenticated},
    {path: '/cards/import', component: CardsImport, beforeEnter: ifAuthenticated},

    {path: '/transactions', component: TransactionsIndex, beforeEnter: ifAuthenticated},
    {path: '/transactions/import', component: TransactionsImport, beforeEnter: ifAuthenticated},
];

export default new VueRouter({
    mode: 'history',
    routes // сокращённая запись для `routes: routes`
});
