// const mocks = {
//     'auth': { 'POST': { token: 'This-is-a-mocked-token' } },
//     'user/me': { 'GET': { name: 'doggo', title: 'sir' } }
// };
export const axios = require('axios');

const apiCall = ({url, data, method}) => new Promise((resolve, reject) => {
    const baseUrl = process.env.VUE_APP_SERVER_URL;

    axios({url: baseUrl + '/api/' + url, data: data, method: method || 'post'})
        .then(resp => {
            console.log(resp.data);
            if (resp.data.status !== 'success') {
                reject(new Error(resp.data.message));
            } else {
                resolve(resp.data.data)
            }
        })
        .catch(err => {
            reject(new Error(err));
        });
});

export default apiCall