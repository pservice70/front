import Vue from 'vue'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faUser, faFile, faShoppingCart , faPercentage, faEye} from '@fortawesome/free-solid-svg-icons'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faUser,faSpinner, faFile, faShoppingCart, faPercentage, faEye);
Vue.component('font-awesome-icon', FontAwesomeIcon);