let path = require('path');

module.exports = {
    runtimeCompiler: true,
    configureWebpack: {
        resolve: {
            alias: {
                utils:  path.resolve(__dirname, 'src/utils/'),
                store:  path.resolve(__dirname, 'src/store/'),
            }
        }
    }
};